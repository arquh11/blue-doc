**Node.js**

Es un entorno de ejecución para JavaScript basado en el motor V8 de Google y actualmente cuenta con el ecosistema de librerias de código abierto mas grande del mundo llamada NPM. Es un entorno de ejecución orientado a eventos.

![](https://gitlab.com/iush/blue-doc/raw/master/images/nodejs.jpg)

----

**Qué es y para que sirve Node.js?**

Node.js es un entorno de ejecución de JavaScript de lado del servidor, el cual permite la creación de aplicaciones de red altamente escalables. 

![](https://gitlab.com/iush/blue-doc/raw/master/images/yellowNodeJs.png)

----

**Qué es y para que sirve Node.js?**

Se basa en Programación Orientada a Eventos Asíncronos. Node.js cambia la manera de como conectarse a un servidor, ya que cada conexión dispara un evento especifico dentro del motor y se afirma que puede soportar decenas de miles de conexiones concurrentes sin bloqueos.

![](https://gitlab.com/iush/blue-doc/raw/master/images/yellowNodeJs.png)

----

**JavaScript en el servidor**

- Lenguaje de programación potente

- Posee un excelente modelo de eventos asincrónicos 

- Curva de aprendizaje reducida debido al previo conocimiento de muchos desarrolladores

![](https://gitlab.com/iush/blue-doc/raw/master/images/yellowNodeJs.png)

----

**Ventajas**

- Gran capacidad de tolerancia a la cantidad de peticiones simultaneas 

- Existencia de gran cantidad de módulos para casi todas las necesidades que se tengan gracias a NPM

- Menor costo de infraestructura

- Comunidad creciente dispuesta a resolver dudas y mostrar nuevas maneras de uso  

![](https://gitlab.com/iush/blue-doc/raw/master/images/nodeacua.png)

----

**Electron.js**

Es un framework desarrollado por GitHub el cual permite el desarrollo de aplicaciones de escritorio usando Node.js y tecnologias para web como HTML, JavaScript y CSS.

![](https://gitlab.com/iush/blue-doc/raw/master/images/electron.png)
