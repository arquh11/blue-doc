***BLUETOOTH LOW ENERGY (BLE)***

El Bluetooth Low Energy, o Bluetooth de baja energía es una tecnología
digital de radio inalámbrica que realiza operaciones entre dispositivos
pequeños autónomos.

![](https://gitlab.com/iush/blue-doc/raw/master/images/BLEPrototyping.png)

----

Es importante saber que los retos enfrentados por la tecnología
bluetooth radican en la descarga de la batería y la vinculación
constante y re-emparejamiento de los dispositivos conectados a través de
ella.

![](https://gitlab.com/iush/blue-doc/raw/master/images/blue-services.png)

----

Del prototipo de bluetooth de baja energía o bluetooth low energy se
desprenden dos modelos desarrollados por el bluetooth special interest
group (SIG) tales como bluetooth Smart y bluetooth Smart Ready.

![](https://gitlab.com/iush/blue-doc/raw/master/images/blue-versions.jpg)

----

**BLUETOOTH SMART**

Bluetooth Smart o Bluetooth inteligente es una tecnología basada en la
funcionalidad de bajo consumo de energía que permite emparejarse con
dispositivos Bluetooth Smart Ready y que se encargan de enviar
información. No necesariamente todo el tiempo deban enviar información
sino en cada momento en que el dispositivo receptor lo solicite.
Mientras no está enviando información el dispositivo puede entrar a un
modo de suspensión, habilitándose inmediatamente le sea solicitada
cierta información o se cumpla alguna regla previamente establecida.

![](https://gitlab.com/iush/blue-doc/raw/master/images/blue-smart.jpg)

----

**BLUETOOTH SMART READY**

Bluetooth Smart Ready o Bluetooth inteligente listo es una tecnología
basada en la funcionalidad de bajo consumo de energía que permite
emparejarse con dispositivos Bluetooth, Bluetooth Smart o de su misma
esencia Bluetooth Smart Ready y que se encargan de enviar y recibir
información de dispositivos que no necesariamente se encuentran
monitoreados. Normalmente lo poseen los dispositivos primarios tales
como teléfonos inteligentes, computadores o tabletas y reciben o envían
información a dispositivos tales como auriculares, accesorios y demás.

![](https://gitlab.com/iush/blue-doc/raw/master/images/blue-smart_1.jpg)

----

**¿Cuáles son las ventajas del Bluetooth de baja energía?**

-   Debido a que las conexiones se realizan mediante radiofrecuencias,
    se eliminan tanto cableado como conectores.

-   Ofrece la posibilidad de crear una red inalámbrica entre
    varios dispositivos.

-   Al ser tecnología BLE, no es un factor importante en el desgaste de
    la batería.

-   Ofrece una velocidad de 24MB/s

-   Es una tecnología de uso global, es decir, en cualquier parte del
    mundo puede ser usada.

----

**¿Cuáles son las desventajas del Bluetooth de baja energía?**

-   La tecnología Bluetooth no ofrece mayor seguridad en cuanto a su
    implementación, en algunas versiones puede ser vulnerable a perdida
    de información.

-   Su alcance es mínimo al momento de intercambiar información. (Máximo
    30 metros).

----

**¿ZIG BEE o BLUETOOTH LOW ENERGY?**

| **ASPECTO**  | **BLUETOOTH**  | **ZIGBEE** |
| :---------: | :-----: | :-------:
| **Distancia**       | En su última versión puede alcanzar hasta 30 metros de alcance.  |   En su última versión puede alcanzar hasta 150 metros de alcance. |
| **Seguridad**       | Ofrece una seguridad moderada.   |  Ofrece una seguridad buena avalada por su uso en la industria. |
| **Uso de batería**  | El uso de batería de la última versión es su valor agregado, pues es mínimo. | El uso de batería de la última versión es el más bajo entre los dispositivos que cumplen su misma función. |
| **Ancho de banda**  | Un ancho de banda regulado, con tendencia a la reducción.   | Un ancho de banda regulado, con tendencia a la reducción. |
| **Confiabilidad**   | Su confiabilidad es mínima cuando existen más de 10 dispositivos conectados por medio de ella. | Es de alta confiabilidad para usar máquina a máquina. |

![](https://gitlab.com/iush/blue-doc/raw/master/images/blue_zigbee.jpg)
